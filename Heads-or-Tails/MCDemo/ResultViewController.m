//
//  ResultViewController.m
//  Rock-Paper-Scissor
//
//  Created by Marcelo Macri on 7/28/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

NSInteger randomNumber;
NSInteger winner;

- (void) setupMySelection{
    [_myNameLabel setText:_myName];
    switch (_mySelectionNumber) {
        case 0:
            [_mySelectionImage setImage:_headImage];
            break;
        case 1:
            [_mySelectionImage setImage:_tailImage];
            break;
        default:
            break;
    }
}

- (void) setupOpponentSelection{
    [_opponentNameLabel setText:_opponentName];
    switch (_opponentSelectionNumber) {
        case 0:
            [_opponentSelectionImage setImage:_headImage];
            break;
        case 1:
            [_opponentSelectionImage setImage:_tailImage];
            break;
        default:
            break;
    }
}

- (void) coinAnimation{
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(setupResultImage)
                                           userInfo:nil
                                            repeats:NO];
    
    NSArray *imageArray;
    imageArray = [[NSArray alloc]initWithObjects:
                  [UIImage imageNamed:@"frame1.png"],
                  [UIImage imageNamed:@"frame2.png"],
                  [UIImage imageNamed:@"frame3.png"],
                  [UIImage imageNamed:@"frame4.png"],
                  [UIImage imageNamed:@"frame5.png"],
                  [UIImage imageNamed:@"frame6.png"],
                  [UIImage imageNamed:@"frame7.png"],
                  [UIImage imageNamed:@"frame8.png"],
                  [UIImage imageNamed:@"frame9.png"],
                  [UIImage imageNamed:@"frame10.png"],
                  [UIImage imageNamed:@"frame11.png"],
                  [UIImage imageNamed:@"frame12.png"],
                  [UIImage imageNamed:@"frame13.png"],
                  [UIImage imageNamed:@"frame14.png"],
                  nil];
    _resultImage.animationImages = imageArray;
    _resultImage.animationDuration = .3;
    [_resultImage startAnimating];
}

- (void) setupResultImage{
    [_resultImage stopAnimating];
        switch (randomNumber) {
            case 0:
                [_resultImage setImage:_headImage];
                break;
            case 1:
                [_resultImage setImage:_tailImage];
                break;
            default:
                break;
        }
        [self determineWinner];
}

- (void) determineWinner{
    if(_mySelectionNumber == randomNumber){
        winner = 1;
    } else {
        winner = 2;
    }
    [self displayWinnerAlert];
}

- (void) displayWinnerAlert {
    // WIN = 1
    // LOSE = 2
    
    UIAlertView *alert;
    
    switch (winner) {
        case 1:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU WIN !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        case 2:
            alert = [[UIAlertView alloc]
                                  initWithTitle:@""
                                  message:@"YOU LOSE !!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles: nil];
            [alert show];
            break;
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _headImage = [UIImage imageNamed:@"frame1.png"];
    _tailImage = [UIImage imageNamed:@"frame8.png"];
    
    NSLog(@"my name: %@", _myName);
    NSLog(@"opponent name: %@", _opponentName);
    NSLog(@"my selection number: %d", _mySelectionNumber);
    NSLog(@"opponent selection number: %d", _opponentSelectionNumber);
    
    randomNumber = arc4random() % 2;
    NSLog(@"ResultViewController: RandomNumber: %d", randomNumber);
    
    [self setupMySelection];
    [self setupOpponentSelection];
    [self coinAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

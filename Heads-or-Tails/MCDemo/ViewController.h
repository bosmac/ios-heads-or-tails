//
//  ViewController.h
//  MCDemo
//
//  Created by Marcelo Macri on 6/6/14.
//  Copyright (c) 2014 marcelomacri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewController.h"

@interface ViewController : UIViewController{
    NSTimer *timer;
}


@property (nonatomic, strong) IBOutlet UIButton *goButton;
@property (nonatomic, strong) IBOutlet UISegmentedControl *headTailSegmentControl;
@property (nonatomic, strong) IBOutlet UIImageView *headTailImageView;

@property (nonatomic, strong) UIImage *headImage;
@property (nonatomic, strong) UIImage *tailImage;

- (IBAction) goButtonPressed;
- (IBAction) headTailSegmentControlPressed;


@end
